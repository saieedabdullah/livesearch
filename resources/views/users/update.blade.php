

@extends('layout.master')
@section('content')

<div class="pt-5"><h3>Update User Data<h3></div>

<form method="post" action="{{route('user.update',$data->id)}}">
    @method('put')
  @csrf
  <div class="mb-3">
    <label class="form-label">Name</label>
    <input type="text" name="name" class="form-control" value="{{$data->name}}">
    @if ($errors->has('name'))

      <span class="text-danger">{{ $errors->first('name') }}</span>

    @endif
  </div>

  <div class="mb-3">
    <label class="form-label">First Name</label>
    <input type="text" name="first_name" class="form-control">
    @if ($errors->has('first_name'))

      <span class="text-danger">{{ $errors->first('first_name') }}</span>

    @endif
  </div>

  <div class="mb-3">
    <label class="form-label">Last Name</label>
    <input type="text" name="last_name" class="form-control">

    @if ($errors->has('last_name'))

      <span class="text-danger">{{ $errors->first('last_name') }}</span>

    @endif

  </div>

  <div class="mb-3">
    <label class="form-label">Company Name</label>
    <input type="text" name="company_name" class="form-control">
    @if ($errors->has('company_name'))

      <span class="text-danger">{{ $errors->first('company_name') }}</span>

    @endif
  </div>

  <div class="mb-3">
    <label class="form-label">Phone Number</label>
    <input type="number" name="phone_number" class="form-control">
    @if ($errors->has('phone_number'))

      <span class="text-danger">{{ $errors->first('phone_number') }}</span>

    @endif
  </div>

  <div class="mb-3">
    <label class="form-label">Address</label>
    <textarea rows="5" type="text" name="address" class="form-control"></textarea>
    @if ($errors->has('address'))

      <span class="text-danger">{{ $errors->first('address') }}</span>

    @endif
  </div>

  <div class="mb-3">
    <label class="form-label">Tax Registration Number</label>
    <input type="number" name="tax_reg_no" class="form-control">
    @if ($errors->has('tax_reg_no'))

      <span class="text-danger">{{ $errors->first('tax_reg_no') }}</span>

    @endif
  </div>

  <div class="mb-3">
      <label for="disabledSelect" class="form-label">Active / Deactive</label>
      <select name="is_active" class="form-select">
        <option value="no">No</option>
        <option value="yes">Yes</option>
      </select>
    </div>

    <div class="mb-3">
        <label for="disabledSelect" class="form-label">Admin option</label>
        <select name="is_admin" class="form-select">
          <option value="no">No</option>
          <option value="yes">Yes</option>
        </select>
      </div>

      <div class="mb-3">
          <label for="disabledSelect" class="form-label">Tag option</label>
          <select name="is_tagged" class="form-select">
            <option value="no">No</option>
            <option value="yes">Yes</option>
          </select>
        </div>

  <div class="mb-3">
    <label class="form-label">Email</label>
    <input type="email" name="email" class="form-control" value="{{$data->email}}">
    @if ($errors->has('email'))

      <span class="text-danger">{{ $errors->first('email') }}</span>

    @endif
  </div>

  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Password</label>
    <input name="password" type="password" class="form-control" id="exampleInputPassword1">
    @if ($errors->has('password'))

      <span class="text-danger">{{ $errors->first('password') }}</span>

    @endif
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection


