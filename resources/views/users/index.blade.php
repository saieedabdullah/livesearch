@extends('layout.master')
@section('content')

<div class="mt-5" >
  <div class="row">
      <div class="col-3">

        <h3>users Table</h3>

      </div>
      <div class="col-6"></div>
      <div class="col-3">

        <a class="btn btn-primary" href="{{route('user.create')}}">Add User
          <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
        </a>

      </div>
  </div>
</div>

<table class="table" >
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Company name</th>
      <th scope="col">Email</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
  @foreach($datas as $data)
      <tr>
        <th>{{$data->first_name}} {{$data->last_name}}</th>
        <td>{{$data->phone_number}}</td>
        <td>{{$data->company_name}}</td>
        <td>{{$data->email}}</td>
        <td>
          <a href="{{route('user.edit', $data->id)}}"  class="btn btn-primary" >Edit</a> /
          <a href="" class="btn btn-danger">Delete</a>
        </td>
      </tr>
  @endforeach
  </tbody>
</table>

@endsection
