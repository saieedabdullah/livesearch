@extends('partials.master')
@section('content')


    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Admin Dashboard') }}
    </h2>


    <div class="col-md-12 pt-2" >

        <div class="row">

            <div class="col-md-3 btn btn-secondary" style=" height:70px;width:100%;margin-left:auto;margin-right:auto">
            
                <a href="{{route('machine.index')}}" style="color:white; text-decoration:none;">
                    <h4 style="margin-top: 10px;">Machine Lists</h4>
                </a>
            
            </div>

            <div class="col-md-3 btn btn-success" style=" height:70px;width:100%;margin-left:auto;margin-right:auto">
            
                <a href="{{route('machine.create')}}" style="color:white; text-decoration:none">
                
                    <h4 style="margin-top: 10px;">Add Machine</h4>
                
                </a>
            
            </div>

            <div class="col-md-3 btn btn-info" style=" height:70px;width:100%;margin-left:auto;margin-right:auto">
            
                <a href="" style="color:white; text-decoration:none">
                    <h4 style="margin-top: 10px;">Invoice</h4>
                </a>
            
            </div>

        </div>

    </div>

    <div class="col-md-12 pt-2" >

        <div class="row">

            <div class="col-md-3 btn btn-secondary" style=" height:70px;width:100%;margin-left:auto;margin-right:auto">
            
                <a href="{{route('user.index')}}" style="color:white; text-decoration:none;">
                    <h4 style="margin-top: 10px;">Users List</h4>
                </a>
            
            </div>

            <div class="col-md-3 btn btn-success" style=" height:70px;width:100%;margin-left:auto;margin-right:auto">
            
                <a href="{{route('user.create')}}" style="color:white; text-decoration:none">
                
                    <h4 style="margin-top: 10px;">Add User</h4>
                
                </a>
            
            </div>

            <div class="col-md-3 btn btn-success" style=" height:70px;width:100%;margin-left:auto;margin-right:auto">
            
                <a href="{{route('user-machine-tag.index')}}" style="color:white; text-decoration:none">
                
                    <h4 style="margin-top: 10px;">Tag User Machine</h4>
                
                </a>
            
            </div>

            

        </div>

    </div>

@endsection





