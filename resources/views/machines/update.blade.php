@extends('layout.master')
@section('content')

<div class="pt-5"><h3>Machine Update form<h3></div>

<form method="post" action="{{route('machine.update',$data->id)}}">
    @method('put')
  @csrf
  <div class="mb-3">
    <label class="form-label">Machine No</label>
    <input type="text" name="machine_no" class="form-control" value="{{$data->machine_no}}">
    @if ($errors->has('machine_no'))

      <span class="text-danger">{{ $errors->first('machine_no') }}</span>

    @endif
  </div>

  <div class="mb-3">
    <label class="form-label">Active status</label>
    <select name="is_active" class="form-select">
      <option value="no">No</option>
      <option value="yes">Yes</option>
    </select>

  </div>
  <div class="mb-3">
    <label class="form-label">Delete status</label>
    <select name="is_delete" class="form-select">
      <option value="no">No</option>
      <option value="yes">Yes</option>
    </select>
  </div>

  <div class="mb-3">
    <label class="form-label">Tag status</label>
    <select name="is_tagged" class="form-select">
      <option value="no">No</option>
      <option value="yes">Yes</option>
    </select>

  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
