@extends('layout.master')
@section('content')

<div class="mt-5" >
  <div class="row">
      <div class="col-3">

        <h3>Machinhe Table</h3>

      </div>
      <div class="col-6"></div>
      <div class="col-3">

        <a class="btn btn-primary" href="{{route('machine.create')}}">Add Machine
          <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
        </a>

      </div>
  </div>
  <input type="search" onkeyup="search()" class="form-control" id="search" placeholder="search...">
</div>

<table class="table" >
  <thead>
    <tr>
      <th scope="col">Machine No</th>
      <th scope="col">Active Status</th>
      <th scope="col">Delete Status</th>
      <th scope="col">Tag Status</th>
      <th scope="col">Option</th>
    </tr>
  </thead>
  <tbody>
    @foreach($datas as $data)
      <tr>
        <th>{{$data->machine_no}}</th>
        <td>{{$data->is_active}}</td>
        <td>{{$data->is_delete}}</td>
        <td>{{$data->is_tagged}}</td>
        <td>
          <a href="{{route('machine.edit',$data->id)}}" class="btn btn-danger">Edit</a>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

<script>
  function search(){
    var search = $('#search').val();
    console.log(search);
  }
</script>

@endsection
