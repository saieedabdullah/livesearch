@extends('partials.master')
@section('content')


    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Admin Dashboard') }}
    </h2>

    <div class="mt-5" >
        <div class="row">
            <div class="col-3">

                <h3> Tag Machine </h3>

            </div>
            <div class="col-6"></div>
            <div class="col-3">

                <a class="btn btn-primary" href="{{route('user-machine-tag.create')}}">Tag Machine with User
                <!-- <i class="fa fa-user" aria-hidden="true"></i> -->
                </a>

            </div>
        </div>
    </div>

    <table class="table" >
        <thead>
            <tr>
            <th scope="col">User Name</th>
            <th scope="col">Machine Name</th>
            <th scope="col">Hourly Session Charge</th>
            <th scope="col">Tag Status</th>
            <th scope="col">Option</th>
            </tr>
        </thead>
        <tbody>
            @foreach($datas as $data)
                <tr>
                    <th>{{$data->name}}</th>
                    <td>{{$data->machine_no}}</td>
                    <td>{{$data->hourly_session_charge}}</td>
                    <td>{{$data->is_taged}}</td>
                    <td>
                        <a href="{{route('user-machine-tag.edit',$data->id)}}" class="btn btn-info">Edit</a> /
                        <a href="{{route('updateTagStatus', $data->id)}}" class="btn btn-info">Detag</a>
                        
                    </td>
                </tr>
            @endforeach    
        </tbody>
    </table>


@endsection