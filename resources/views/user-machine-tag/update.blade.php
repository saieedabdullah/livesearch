@extends('partials.master')
@section('content')



<div class="card" style="width: 100%">
  <div class="card-header">
    Update Session Rate
  </div>
  <ul class="list-group list-group-flush">
    <form method="post" action="{{route('user-machine-tag.update',$data->id)}}">
        @method('put')
        @csrf
        <div class="form-group">
            {{-- <label for="exampleInputEmail1">Hourly Session Charge</label> --}}
            <input name="hourly_session_charge" type="text" class="form-control" placeholder="Session Rate">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </ul>
</div>

@endsection