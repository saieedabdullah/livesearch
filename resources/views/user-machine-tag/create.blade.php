@extends('partials.master')
@section('content')

<form method="post" action="{{route('user-machine-tag.store')}}">
    @csrf
    <div class="form-group">
        <label >Select User</label>
        <select class="form-control" name="user_id">
        @foreach($users as $user)
            <option value="{{$user->id}}">{{$user->name}}</option>       
        @endforeach
        </select>
  </div>
  <div class="form-group">
    <label >Select Machine</label>
    <select multiple class="form-control" name="machine_id[]">
        @foreach($machines as $machine)
            <option value="{{$machine->id}}">{{$machine->machine_no}}</option>
        @endforeach
    </select>
  </div>

     <div class="form-group">
        <label for="exampleFormControlSelect1">Select Machine</label>
        <select class="form-control" id="exampleFormControlSelect1" name="currency">
            <option value="taka">Taka</option>
            <option value="doller">Doller</option>
            <option value="diner">Diner</option>
            <option value="pound">Pound</option>  
        </select>
  </div>

   <div class="form-group">
    <label for="exampleInputEmail1">Hourly Session Charge</label>
    <input name="hourly_session_charge" type="text" class="form-control">
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection