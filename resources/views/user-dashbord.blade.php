@extends('partials.master')
@section('content')


    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User Dashboard') }}
    </h2>

    <div class="card" style="width: 100%;">
        <div class="card-header">
            User Profile
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">Name: {{$data->name}}</li>
            <li class="list-group-item">First Name: {{$data->first_name}}</li>
            <li class="list-group-item">Last Name: {{$data->last_name}}</li>
            <li class="list-group-item">Company Name: {{$data->company_name}}</li>
            <li class="list-group-item">Address: {{$data->address}}</li>
            <li class="list-group-item">Email: {{$data->email}}</li>
        </ul>
    </div>

@endsection





