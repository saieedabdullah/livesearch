<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachineUserTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('machine_user_tags', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('machine_id');
            $table->double('hourly_session_charge', 5, 2);
            $table->string('currency');
            $table->string('is_taged')->default('Yes');
            $table->dateTime('tagged_at', $precision = 0);
            $table->dateTime('detagged_at', $precision = 0)->nullable();
            $table->unsignedBigInteger('tagged_by')->nullable();
            $table->unsignedBigInteger('detagged_by')->nullable()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('machine_id')->references('id')->on('machines');
            $table->foreign('tagged_by')->references('id')->on('users');
            $table->foreign('detagged_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('machine_user_tags');
    }
}
