<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\machine;
use App\Models\User;
use App\Models\MachineUserTag;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;


class UserMachineTag extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = DB::table('users')
            ->join('machine_user_tags', 'users.id', '=', 'machine_user_tags.user_id')
            ->join('machines', 'machines.id', '=', 'machine_user_tags.machine_id')
            ->select('machine_user_tags.hourly_session_charge', 'machine_user_tags.currency', 'machine_user_tags.id', 'machine_user_tags.is_taged', 'machine_user_tags.tagged_at', 'users.name', 'machines.machine_no')
            ->where(['is_taged' => 'Yes'])->get();



        return view('user-machine-tag.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $machines = DB::table('machine_user_tags')
            ->join('machines', 'machines.id', '=', 'machine_user_tags.machine_id')
            ->select('machines.machine_no', 'machines.id')
            ->where(['is_taged' => 'No'])->get();

        $users = User::all();

        return view('user-machine-tag.create', compact('machines', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->machine_id);

        $inputs = $request->all();


        foreach ($inputs['machine_id'] as $key => $value) {
            $data = new MachineUserTag;
            $data->user_id = $request->user_id;
            $data->machine_id = $request->machine_id[$key];
            $data->hourly_session_charge = $request->hourly_session_charge;
            $data->currency = $request->currency;
            $data->tagged_at = Carbon::now()->addDays(30)->format('Y-m-d H:i:s');
            $data->tagged_by = Auth::id();
            $data->save();
            return Redirect()->route('user-machine-tag.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MachineUserTag::find($id);
        return view('user-machine-tag.update', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'hourly_session_charge' => 'numeric',
        ]);

        $data = MachineUserTag::find($id);
        $data->hourly_session_charge = $request->hourly_session_charge;
        $data->save();
        return Redirect()->route('user-machine-tag.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function changeTagStatus($id)
    {
        $tag = MachineUserTag::find($id);

        MachineUserTag::where('id', $id)->update(['is_taged' => 'No']);
        return Redirect()->route('user-machine-tag.index');
    }
}
