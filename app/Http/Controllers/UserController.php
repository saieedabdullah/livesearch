<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = User::all();
        return view('users.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            'name' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'company_name' => 'required',
            'phone_number' => 'required|max:14',
            'address' => 'required',
            'tax_reg_no' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);



        $data = new User;
        $data->name = $request->name;
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->company_name = $request->company_name;
        $data->phone_number = $request->phone_number;
        $data->address = $request->address;
        $data->tax_reg_no = $request->tax_reg_no;
        $data->is_active = $request->is_active;
        $data->is_admin = $request->is_admin;
        $data->is_tagged = $request->is_tagged;
        $data->email = $request->email;
        $data->password = bcrypt($data->password);
        $data->save();
        return Redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = User::find($id);
        return view('users.update', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = $this->validate($request, [
            'phone_number' => 'max:14',
        ]);



        $data =  User::find($id);
        $data->name = $request->name;
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->company_name = $request->company_name;
        $data->phone_number = $request->phone_number;
        $data->address = $request->address;
        $data->tax_reg_no = $request->tax_reg_no;
        $data->is_active = $request->is_active;
        $data->is_admin = $request->is_admin;
        $data->is_tagged = $request->is_tagged;
        $data->email = $request->email;
        $data->save();
        return Redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
