<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;


class DashboardController extends Controller
{
    public function index()
    {

        // dd('Inside index');
        if (Auth::user()->hasRole('admin')) {
            return view('admin-dashboard');
        } elseif (Auth::user()->hasRole('user')) {
            $data = Auth::user();
            return view('user-dashbord', compact('data'));
        } else {

            return view('layout.master');
        }
    }
}
