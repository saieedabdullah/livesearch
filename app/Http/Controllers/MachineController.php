<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\machine;

class MachineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = machine::all();
        return view('machines.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('machines.create_machine');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'machine_no' => 'required|unique:machines'
        ]);
        $data = new machine;
        $data->machine_no = $request->machine_no;
        $data->is_active = $request->is_active;
        $data->is_delete = $request->is_delete;
        $data->is_tagged = $request->is_tagged;
        $data->save();
        return Redirect()->route('machine.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = machine::find($id);
        return view('machines.update', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'machine_no' => 'unique:machines'
        ]);
        $data = machine::find($id);
        $data->machine_no = $request->machine_no;
        $data->is_active = $request->is_active;
        $data->is_delete = $request->is_delete;
        $data->is_tagged = $request->is_tagged;
        $data->save();
        return Redirect()->route('machine.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
